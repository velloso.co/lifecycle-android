package co.velloso.android.lifecycle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlin.reflect.KClass

fun <T : ViewModel> FragmentActivity.getViewModel(
        modelClass: KClass<T>,
        owner: FragmentActivity = this
): T = ViewModelProvider(owner).get(modelClass.java)

fun <T : ViewModel> Fragment.getViewModel(
        modelClass: KClass<T>,
        owner: Fragment = this
): T = ViewModelProvider(owner).get(modelClass.java)

inline fun <reified T : ViewModel> FragmentActivity.viewModel() = lazy {
    getViewModel(T::class, this)
}

inline fun <reified T : ViewModel> Fragment.viewModel() = lazy {
    getViewModel(T::class, this)
}

inline fun <reified T : ViewModel> Fragment.activityViewModel() = lazy {
    (activity ?: throw RuntimeException("Activity not initialized yet.")).getViewModel(T::class)
}