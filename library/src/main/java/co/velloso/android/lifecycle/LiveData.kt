package co.velloso.android.lifecycle

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations


@MainThread
fun <X, Y> LiveData<X>.map(mapFunction: (X) -> Y): LiveData<Y> {
    return Transformations.map(this) { mapFunction.invoke(it) }
}

@MainThread
fun <X, Y> LiveData<X>.switchMap(switchMapFunction: (X) -> LiveData<Y>): LiveData<Y> {
    return Transformations.switchMap(this) { switchMapFunction.invoke(it) }
}
